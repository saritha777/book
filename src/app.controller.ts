import { CACHE_MANAGER, Controller, Get, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { Cache } from 'cache-manager';
import { Profile } from './book/entity/profile';

@Controller()
export class AppController {
  fakeValue = 'my name is ....';
  fakeModel: Profile = {
    name: 'saritha',
    email: 'saritha@gmail.com'
  };
  constructor(
    private readonly appService: AppService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('get-string-cache')
  async getSimpleString() {
    const value = await this.cacheManager.get('my-string');
    if (value) {
      return {
        data: value,
        LoadsFrom: 'redis cache'
      };
    }
    await this.cacheManager.set('my-string', this.fakeValue, { ttl: 60 });
    return {
      data: this.fakeValue,
      LoadsFrom: 'fake database'
    };
  }

  @Get('fetch-object-cache')
  async getCache() {
    const profile = await this.cacheManager.get<Profile>('my-object');
    if (profile) {
      return {
        data: profile,
        loadsFrom: 'cache'
      };
    }

    await this.cacheManager.set<Profile>('my-object', this.fakeModel, {
      ttl: 60
    });
    return {
      data: this.fakeModel,
      loadFrom: 'fake data base'
    };
  }
}
