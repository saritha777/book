import {
  BadRequestException,
  Injectable,
  Logger,
  Res,
  UnauthorizedException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Book } from './entity/book.entity';
import { BookEntries } from './entity/bookEntries.entity';
import { UserLogin } from './entity/login.entity';
import { User } from './entity/person.entity';
import * as bcrypt from 'bcryptjs';
import { Response, Request } from 'express';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class BookService {
  logger: Logger;
  constructor(
    @InjectRepository(UserLogin)
    private loginRepository: Repository<UserLogin>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Book)
    private bookRepository: Repository<Book>,
    @InjectRepository(BookEntries)
    private entryRepository: Repository<BookEntries>,
    private jwtService: JwtService
  ) {
    this.logger = new Logger(BookService.name);
  }

  async addUser(data: User): Promise<User> {
    const pass = await bcrypt.hash(data.password, 10);
    const user = new User();
    const login = new UserLogin();
    user.name = login.name = data.name;
    user.password = login.password = pass;
    user.phoneNo = data.phoneNo;
    user.email = data.email;
    user.addressline1 = data.addressline1;
    user.addressline2 = data.addressline2;
    user.state = data.state;
    user.country = data.country;
    user.pinCode = data.pinCode;
    user.login = login;

    //  this.logger.log('user added successfully');
    return this.userRepository.save(user);
  }

  //checking user is logined or not
  async loginUser(data: UserLogin, @Res() response: Response) {
    const user = await this.userRepository.findOne({ name: data.name });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('pasward incorrect');
    }
    const jwt = await this.jwtService.signAsync({ id: user.id });
    response.cookie('jwt', jwt, { httpOnly: true });
    return {
      message: 'success'
    };
  }

  // find user using cookie
  async findUser(request: Request) {
    try {
      const cookie = request.cookies['jwt'];

      const data = await this.jwtService.verifyAsync(cookie);
      if (!data) {
        throw new UnauthorizedException();
      }
      const user = await this.userRepository.findOne({ id: data['id'] });
      const { password, ...result } = user;
      return result;
    } catch (e) {
      throw new UnauthorizedException();
    }
  }

  addBook(bookdata: Book) {
    const book = new Book();
    book.bookName = bookdata.bookName;
    book.bookAuthor = bookdata.bookAuthor;
    book.ISBNNumber = bookdata.ISBNNumber;
    book.noOfPages = bookdata.noOfPages;

    //  this.logger.log('book added successfully');
    return this.bookRepository.save(book);
  }

  async addBookEntries(info: BookEntries) {
    const userData: User = await this.userRepository.findOne({
      name: info.userName
    });
    const data = new BookEntries();
    data.userName = info.userName;
    data.bookISBN = info.bookISBN;
    data.dateOfFinishing = info.dateOfFinishing;
    data.review = info.review;
    data.URL = info.URL;
    data.addUser(userData);

    const bookData = await this.bookRepository.findOne({
      ISBNNumber: info.bookISBN
    });
    info.bookName = bookData.bookName;
    data.bookName = bookData.bookName;

    //  this.logger.log('book entry added successfully');
    return await this.entryRepository.save(info);
  }

  //get methods for user
  getAllUsers(): Promise<User[]> {
    this.logger.log('Getting all users');
    return this.userRepository.find();
  }

  getUserById(id: number) {
    this.logger.log('Getting  user with id');
    return this.userRepository.findOne({ id: id });
  }

  getUserByName(name: string) {
    this.logger.log('Getting user with name');
    return this.userRepository.findOne({ name: name });
  }

  // get methods for book
  getAllBooks() {
    this.logger.log('Getting all Books');
    return this.bookRepository.find();
  }

  getBookById(id: number) {
    this.logger.log('Getting bokk with id');
    return this.bookRepository.findOne({ bookId: id });
  }

  getBookByName(name: string) {
    this.logger.log('Getting book with name');
    return this.bookRepository.findOne({ bookName: name });
  }

  //get methods for book entries
  getAllBookEntries() {
    this.logger.log('Getting all Book Entries');
    return this.entryRepository.find();
  }

  getBookEntrieByUserName(name: string) {
    this.logger.log('Getting Book Entries with user name');
    return this.entryRepository.findOne({ userName: name });
  }

  deleteUserById(id: number) {
    this.logger.log('Successfully deleted user with id');
    return this.userRepository.delete({ id });
  }

  deleteBookById(id: number) {
    this.logger.log('Successfully deleted book with id');
    return this.bookRepository.delete({ bookId: id });
  }

  async deleteBookEntryById(id: number) {
    this.logger.log('Successfully deleted book entries with id');
    return await this.entryRepository.delete({ id });
  }

  updateUser(id: number, data: User) {
    const user = new User();
    const login = new UserLogin();
    user.name = login.name = data.name;
    user.password = login.password = data.password;
    user.phoneNo = data.phoneNo;
    user.email = data.email;
    user.addressline1 = data.addressline1;
    user.addressline2 = data.addressline2;
    user.state = data.state;
    user.country = data.country;
    user.pinCode = data.pinCode;
    user.login = login;
    this.loginRepository.update({ id: id }, login);
    this.userRepository.update({ id: id }, data);
    this.logger.log('Successfully updated user with id');
    return 'user details updated sucessfully';
  }

  updateBook(id: number, bookdata: Book) {
    const book = new Book();
    book.bookName = bookdata.bookName;
    book.bookAuthor = bookdata.bookAuthor;
    book.ISBNNumber = bookdata.ISBNNumber;
    book.noOfPages = bookdata.noOfPages;

    this.bookRepository.update({ bookId: id }, book);
    this.logger.log('Successfully updated book with id');
    return 'book details updated sucessfully';
  }

  async updateBookEntry(id: number, info: BookEntries) {
    const userData: User = await this.userRepository.findOne({
      name: info.userName
    });
    const data = new BookEntries();
    data.userName = info.userName;
    data.bookISBN = info.bookISBN;
    data.dateOfFinishing = info.dateOfFinishing;
    data.review = info.review;
    data.URL = info.URL;
    data.addUser(userData);

    const bookData = await this.bookRepository.findOne({
      ISBNNumber: info.bookISBN
    });
    info.bookName = bookData.bookName;
    data.bookName = bookData.bookName;

    this.entryRepository.update({ id: id }, info);
    this.logger.log('Successfully updated book entry with id');
    return 'Entry updated sucessfully';
  }

  //logout user
  async logout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return {
      message: 'logout'
    };
  }
}
