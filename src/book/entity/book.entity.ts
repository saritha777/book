import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { BookEntries } from './bookEntries.entity';

@Entity()
export class Book {
  @PrimaryGeneratedColumn()
  bookId: number;

  @ApiProperty({
    type: String,
    description: 'The name of the book',
    default: ''
  })
  @IsString()
  @Column()
  bookName: string;

  @ApiProperty({
    type: String,
    description: 'The author of the book',
    default: ''
  })
  @IsString()
  @Column()
  bookAuthor: string;

  @ApiProperty({
    type: Number,
    description: 'The ISBN Number of the book',
    default: ''
  })
  @IsInt()
  @Column()
  ISBNNumber: number;

  @ApiProperty({
    type: Number,
    description: 'The noOfPages of the book',
    default: ''
  })
  @IsString()
  @Column()
  noOfPages: number;
}
