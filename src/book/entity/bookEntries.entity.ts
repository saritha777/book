import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { Book } from './book.entity';
import { User } from './person.entity';

@Entity()
export class BookEntries {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: ''
  })
  @IsString()
  @Column()
  userName: string;

  @ApiProperty({
    type: Number,
    description: 'The address of the user',
    default: ''
  })
  @IsInt()
  @Column()
  bookISBN: number;

  @Column()
  bookName: String;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: ''
  })
  @IsString()
  @Column()
  dateOfFinishing: string;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: ''
  })
  @IsString()
  @Column()
  review: string;

  @ApiProperty({
    type: String,
    description: 'The address of the user',
    default: ''
  })
  @IsString()
  @Column()
  URL: string;

  @ManyToMany(() => User, (user) => user.entry, {
    cascade: true,
    onUpdate: 'CASCADE'
  })
  @JoinTable()
  user: User[];

  async addUser(user: User): Promise<User> {
    if (this.user == null) {
      this.user = new Array<User>();
    }
    this.user.push(user);
    return user;
  }
}
