import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsInt, IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn
} from 'typeorm';
import { BookEntries } from './bookEntries.entity';
import { UserLogin } from './login.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @IsString()
  @Column({ unique: true })
  name: string;

  @ApiProperty()
  @IsString()
  @Column()
  password: string;

  @ApiProperty()
  @IsInt()
  @Column()
  phoneNo: number;

  @ApiProperty()
  @IsString()
  @Column()
  email: String;

  @ApiProperty()
  @IsString()
  @Column()
  addressline1: String;

  @ApiProperty()
  @IsString()
  @Column()
  addressline2: String;

  @ApiProperty()
  @IsString()
  @Column()
  state: String;

  @ApiProperty()
  @IsString()
  @Column()
  country: String;

  @ApiProperty()
  @IsInt()
  @Column()
  pinCode: Number;

  @OneToOne(() => UserLogin, (login) => login.user, {
    cascade: true
  })
  login: UserLogin;

  @ManyToMany(() => BookEntries, (entry) => entry.user)
  entry: BookEntries;
}
