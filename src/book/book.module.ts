import { CacheModule, Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookController } from './book.controller';
import { BookService } from './book.service';
import { Book } from './entity/book.entity';
import { BookEntries } from './entity/bookEntries.entity';
import { UserLogin } from './entity/login.entity';
import { User } from './entity/person.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, UserLogin, Book, BookEntries]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' }
    }),
    CacheModule.register()
  ],
  controllers: [BookController],
  providers: [BookService]
})
export class BookModule {}
