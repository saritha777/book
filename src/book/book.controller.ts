import {
  Body,
  CACHE_MANAGER,
  Controller,
  Delete,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Param,
  Post,
  Put,
  Req,
  Res,
  UseFilters
} from '@nestjs/common';
import { HttpExceptionFilter } from '../filters/http-exception.filter';
import { BookService } from './book.service';
import { Book } from './entity/book.entity';
import { BookEntries } from './entity/bookEntries.entity';
import { User } from './entity/person.entity';
import { Profile } from './entity/profile';
import { Cache } from 'cache-manager';
import { UserLogin } from './entity/login.entity';
import { Response, Request } from 'express';
import { customException } from '../customeException/custom.exception';

@Controller()
export class BookController {
  constructor(
    private readonly bookService: BookService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache
  ) {}

  @Post('/user')
  addUser(@Body() data: User): Promise<User> {
    return this.bookService.addUser(data);
  }

  // checking user is logined or not
  @Post('/login')
  async login(
    @Body() login: UserLogin,
    @Res({ passthrough: true }) response: Response
  ) {
    return this.bookService.loginUser(login, response);
  }

  //find user using cookie
  @Get('/userlogin')
  async user(@Req() request: Request) {
    return this.bookService.findUser(request);
  }

  // logout user
  @Post('/logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    return this.bookService.logout(response);
  }

  @Post('/book')
  addBook(@Body() data: Book): Promise<Book> {
    return this.bookService.addBook(data);
  }
  @Post('/bookEntries')
  addBookEntires(@Body() data: BookEntries): Promise<BookEntries> {
    return this.bookService.addBookEntries(data);
  }

  //user Get methods
  @Get('/user')
  async getAllUsers(): Promise<User[]> {
    return this.bookService
      .getAllUsers()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'no users, please add one user',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'no users, please add one user',
          HttpStatus.NOT_FOUND
        );
      });
  }

  @Get('/user/id/:id')
  getUserById(@Param('id') id: number): Promise<User> {
    return this.bookService
      .getUserById(id)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException('user not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new HttpException('user not found', HttpStatus.NOT_FOUND);
      });
  }

  @Get('/user/name/:name')
  getUserByName(@Param('name') name: string): Promise<User> {
    return this.bookService
      .getUserByName(name)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'user name not found',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException('user name not found', HttpStatus.NOT_FOUND);
      });
  }

  // book get methods
  @Get('/book')
  getAllBooks(): Promise<Book[]> {
    return this.bookService
      .getAllBooks()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'Book store is empty',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException('Book store is empty', HttpStatus.NOT_FOUND);
      });
  }

  @Get('/book/:id')
  //  @UseFilters(new HttpExceptionFilter())
  getBookById(@Param('id') id: number): Promise<Book> {
    return this.bookService
      .getBookById(id)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException('book not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new customException('book not found', HttpStatus.NOT_FOUND);
      });
  }

  @Get('/book/name/:name')
  getBookByName(@Param('name') name: string): Promise<Book> {
    return this.bookService
      .getBookByName(name)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'book name not found',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException('book name not found', HttpStatus.NOT_FOUND);
      });
  }

  // book entries get methods
  @Get('/bookEntries')
  getAllBookEntries(): Promise<BookEntries[]> {
    return this.bookService
      .getAllBookEntries()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException(
            'it is empty please add one entry',
            HttpStatus.NOT_FOUND
          );
        }
      })
      .catch(() => {
        throw new customException(
          'it is empty please add one entry',
          HttpStatus.NOT_FOUND
        );
      });
  }

  @Get('/bookEntries/name/:name')
  getBookEntriesByUsername(@Param('name') name: string): Promise<BookEntries> {
    return this.bookService
      .getBookEntrieByUserName(name)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new customException('username not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new customException('username not found', HttpStatus.NOT_FOUND);
      });
  }

  //delete method for user
  @Delete('/user/:id')
  deleteUserById(@Param('id') id: number) {
    return this.bookService.deleteUserById(id);
  }

  //delete method for book
  @Delete('/book/:id')
  deleteBookById(@Param('id') id: number) {
    return this.bookService
      .deleteBookById(id)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException('book id not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new HttpException('book id not found', HttpStatus.NOT_FOUND);
      });
  }

  //delete method for book entry
  @Delete('/bookEntries/:id')
  deleteBookEntryById(@Param('id') id: number) {
    return this.bookService.deleteBookEntryById(id);
  }

  //update method for user
  @Put('/user/:id')
  updateUser(@Param('id') id: number, @Body() user: User) {
    return this.bookService.updateUser(id, user);
  }

  //update method for book
  @Put('/book/:id')
  updateBook(@Param('id') id: number, @Body() book: Book) {
    return this.bookService.updateBook(id, book);
  }

  //update method for book entry
  @Put('/bookEntries/:id')
  updateBookEntry(@Param('id') id: number, @Body() bookEntry: BookEntries) {
    return this.bookService.updateBookEntry(id, bookEntry);
  }
}
