import { HttpException, HttpStatus } from '@nestjs/common';

export class customException extends HttpException {
  constructor(error: string, httpstatus: HttpStatus) {
    super(error, httpstatus);
  }
}
